## Development Environment Setup on Ubuntu based OSes

- Download and unpack `Android Studio` (`*.tar.gz` file) from this
[website](https://developer.android.com/studio) into `/usr/local`

    cd ~/Downloads
    sudo tar -xf android-studio-*.tar.gz -C /usr/local

- Check that Android Studio has successfully been downloaded and copied:
  `ls /usr/local/ | grep android-studio`
- Run the installation script: `/usr/local/android-studio/bin/studio.sh`
- Configure the installation:

![as-0](./as-0.png)

Select the "Standard" radio button and press the "Next" button.

![as-1](./as-1.png)

Select the radio button ("Dracula"/"Light") and press the "Next" button according to your personal preference.

![as-2](./as-2.png)

The SDK folder (`/home/<USER>/Android/Sdk`) will differ according to your Linux
user name.

![as-3](./as-3.png)

Accept the license agreement by selecting the "Accept" radio button and pressing
the "Next" button.

![as-4](./as-4.png)

The device emulation is supported out-of-the-box without acceleration.
On most systems (like one the system the screenshot was taken on) it should be possible to enable optional acceleration.
To do so click the link in the help text to get more information.
Click the "Finish" button to continue the installation procedure.

![as-5](./as-5.png)

![as-6](./as-6.png)

- Configure a device emulator:

Android Studio will be started and some software will be loaded in the background indicated by a progess bar in the lower right corner of the window. 
Wait until all software has been loaded in the background.

![as-7](./as-7.png)

In the upper right area of the window you'll find a "No Devices" dropdown selector.
Press on it, select the "Virtual" tab and press the button "Create device".

![as-8](./as-8.png)

Select a hardware device you want to emulate and press the "Next" button.

![as-9](./as-9.png)

In addition to the hardware device you have to select the Android version via a system image.
To do so download one of them by pressing the corresponding "Download" link.
Ater downloading you can continue by pressing the "Next" button.

![as-10](./as-10.png)

![as-11](./as-11.png)

![as-12](./as-12.png)

![as-emulator-1](./as-emulator-1.png)

![as-emulator-2](./as-emulator-2.png)

You can select the configured (and potential additional emulators) in the upper right area of the window.

![as-emulator-3](./as-emulator-3.png)

For later reconfiguration you can access the device emulator on the upper right area of the window.

![as-emulator-4](./as-emulator-4.png)

You'll see the device emulator on the right side of the windo.

![as-emulator-5](./as-emulator-5.png)
