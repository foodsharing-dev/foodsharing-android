package de.foodsharing.notifications

import android.content.Context
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import com.google.capillary.Config
import com.google.capillary.android.KeyManager
import com.google.capillary.android.WebPushKeyManager
import java.io.IOException
import java.security.GeneralSecurityException

/**
 * Helper methods encapsulating functionality related to Capillary
 */
object CapillaryUtils {
    val WEB_PUSH_KEYCHAIN_ID = "web_push_keychain"

    /**
     * Initializes the Android security provider and the Capillary library.
     */
    internal fun initialize(context: Context) {
        updateAndroidSecurityProvider(context)
        try {
            // Initializes crypto related stuff
            Config.initialize()
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        }
    }

    private fun updateAndroidSecurityProvider(context: Context) {
        try {
            ProviderInstaller.installIfNeeded(context)
        } catch (e: GooglePlayServicesRepairableException) {
            // Indicates that Google Play services is out of date, disabled, etc.
            e.printStackTrace()
            // Prompt the user to install/update/enable Google Play services.
            GoogleApiAvailability.getInstance()
                .showErrorNotification(context, e.connectionStatusCode)
        } catch (e: GooglePlayServicesNotAvailableException) {
            // Indicates a non-recoverable error; the ProviderInstaller is not able
            // to install an up-to-date Provider.
            e.printStackTrace()
        }
    }

    /**
     * Creates a Capillary key manager for the specified key algorithm.
     */
    @Throws(IOException::class, GeneralSecurityException::class)
    internal fun getKeyManager(context: Context): KeyManager {
        return WebPushKeyManager.getInstance(context, WEB_PUSH_KEYCHAIN_ID)
    }
}
