package de.foodsharing.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.foodsharing.notifications.FoodsharingMessagingService

@Module
abstract class FlavorActivitiesModule {
    @ContributesAndroidInjector
    abstract fun bindFoodsharingMessagingService(): FoodsharingMessagingService
}
