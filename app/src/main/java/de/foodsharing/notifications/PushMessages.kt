package de.foodsharing.notifications

import com.google.gson.annotations.SerializedName
import de.foodsharing.model.MessageResponse
import de.foodsharing.model.User

const val CONVERSATION_PUSH_MESSAGE_TYPE = "c"
const val DEFAULT_PUSH_MESSAGE_TYPE = "d"

sealed class PushMessage(
    @SerializedName("t")
    val type: String
)

data class ConversationPushMessage(
    @SerializedName("c")
    val conversationId: Int,
    @SerializedName("m")
    val message: MessageResponse,
    @SerializedName("a")
    val author: User
) : PushMessage(CONVERSATION_PUSH_MESSAGE_TYPE)

data class DefaultPushMessage(
    @SerializedName("c")
    val caption: String,
    @SerializedName("b")
    val body: String,
    @SerializedName("u")
    val url: String?
) : PushMessage(DEFAULT_PUSH_MESSAGE_TYPE)
