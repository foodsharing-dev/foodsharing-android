package de.foodsharing.model

import java.io.Serializable

data class Store(
    val id: Int,
    val name: String
) : Serializable
