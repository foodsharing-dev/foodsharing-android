package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import java.util.Date

/**
 * An entry of the content (CMS) module.
 */
data class ContentEntry(
    val title: String,
    var body: String?,
    @SerializedName("lastModified")
    val lastModified: Date
)
