package de.foodsharing.model

enum class NotificationType(val identifierPrefix: String, val titleKey: String) {
    /**
     * The user has a new friend request. Arguments: IDs of this user and the sender of the request.
     */
    BUDDY_REQUEST("buddy", "buddy_request"),

    /**
     * A new post was written on the wall of an FSP which the user is following. Argument: ID of the FSP.
     */
    FOOD_SHARE_POINT_POST("fairteiler", "ft_update"),

    /**
     * Notification for ambassadors about a new FSP proposal. Argument: ID of the FSP.
     */
    NEW_FOOD_SHARE_POINT("new-fairteiler", "sharepoint_activate"),

    /**
     * A new forum post in a thread the user is participating in. Argument: ID of the post.
     */
    NEW_FORUM_POST("forum-post", "forum_post"),

    /**
     * Notification for ambassadors about a new foodsaver. Argument: the foodsaver"s ID.
     */
    NEW_FOODSAVER_IN_REGION("new-fs", "new_foodsaver"),

    /**
     * Notification to user for the verification status. Argument: the foodsaver's ID.
     */
    FOODSAVER_VERIFIED("fs-verified-%d", "foodsaver_verified"),

    /**
     * The creation of the foodsaver"s pass has failed.
     */
    PASS_CREATION_FAILED("pass-fail", "passgen_failed"),

    /**
     * Notification for a store manager that someone wants to join a store.
     */
    NEW_STORE_REQUEST("store-request", "store_new_request"),

    /**
     * The user"s store request was accepted.
     */
    STORE_REQUEST_ACCEPTED("store-arequest", "store_request_accept"),

    /**
     * The user"s store request was rejected.
     */
    STORE_REQUEST_REJECTED("store-drequest", "store_request_deny"),

    /**
     * The user was put on the waiting list (jumper), of a store.
     */
    STORE_REQUEST_WAITING("store-wrequest", "store_request_accept_wait"),

    /**
     * The user was added to a store without a request.
     */
    ADDED_TO_STORE_WITHOUT_REQUEST("store-imposed", "store_request_imposed"),

    /**
     * Notification for a store manager that there are unconfirmed pickups.
     */
    STORE_UNCONFIRMED_PICKUP("store-fetch-unconfirmed", "betrieb_fetch"),

    /**
     * A new store was created.
     */
    NEW_STORE("store-new", "store_new"),

    /**
     * The pickup times in a store were changed.
     */
    STORE_TIME_CHANGED("store-time", "store_cr_times"),

    /**
     * A new post was written on the wall of a store.
     */
    STORE_WALL_POST("store-wallpost", "store_wallpost"),

    /**
     * A new blog entry is created and needs to be checked.
     */
    NEW_BLOG_POST("blog-check", "blog_new_check"),

    /**
     * A new poll was created in a region or work group.
     */
    NEW_POLL("new-poll", "poll_new"),

    /**
     * The user"s request to join a work group was accepted.
     */
    WORKING_GROUP_REQUEST_ACCEPTED("workgroup-arequest", "workgroup_request_accept"),

    /**
     * The user"s request to join a work group was denied.
     */
    WORKING_GROUP_REQUEST_DENIED("workgroup-drequest", "workgroup_request_decline"),

    /**
     * A new report for a user was created.
     */
    NEW_REPORT("new-report", "new_report"),

    /**
     * Unknown type. This value will only be used by [Notification.getType] if the notification's type string is not listed here.
     */
    UNKNOWN("unknown", "---")
}
