package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date

/**
 * A basket is a collection of some food that a user wants to give away. People can upload a
 * description of that food, optionally including a picture. The basket contains informations about
 * where to pick up the food and how to contact the user.
 */
data class Basket(
    val id: Int,
    val description: String,
    var picture: String?,
    @SerializedName("createdAt")
    val createdAt: Date,
    @SerializedName("updatedAt")
    val updatedAt: Date,
    @SerializedName("contactTypes")
    val contactTypes: Array<Int>,
    val until: Date,
    override val lat: Double,
    override val lon: Double,
    @SerializedName("requestCount")
    val requestCount: Int,
    val requests: List<BasketRequest>,
    val creator: User,
    @SerializedName("tel")
    val phone: String,
    @SerializedName("handy")
    val mobile: String
) : Serializable, ICoordinate {
    companion object {
        const val CONTACT_TYPE_MESSAGE = 1
        const val CONTACT_TYPE_PHONE = 2
    }
}
