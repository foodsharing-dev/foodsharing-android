package de.foodsharing.model

import java.io.Serializable

/**
 * Represents the marker of a community on the map. When clicked, the region's id is used to fetch the community
 * description.
 */
data class CommunityMarker(
    val id: Int,
    override val lat: Double,
    override val lon: Double
) : Serializable, ICoordinate
