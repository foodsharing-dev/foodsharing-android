package de.foodsharing.model

data class PublicKey(
    val publicKey: String,
    val authSecret: String,
    val keychainUniqueId: String,
    val serialNumber: Int
)

data class PushSubscription(
    val publicKey: PublicKey,
    val fcmToken: String
)
