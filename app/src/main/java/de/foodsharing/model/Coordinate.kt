package de.foodsharing.model

import android.os.Parcelable
import de.foodsharing.utils.OsmdroidUtils.toGeoPoint
import kotlinx.parcelize.Parcelize

/**
 * Interface for any object that has a coordinate.
 */
interface ICoordinate {
    val lat: Double
    val lon: Double

    fun toCoordinate(): Coordinate {
        return Coordinate(lat, lon)
    }
}

/**
 * Object representing a coordinate.
 */
@Parcelize
data class Coordinate(
    override val lat: Double,
    override val lon: Double
) : ICoordinate, Parcelable {

    /**
     * Returns the distance in meters to the given coordinate.
     */
    fun distanceTo(coord: Coordinate): Double {
        return toGeoPoint().distanceToAsDouble(coord.toGeoPoint())
    }
}
