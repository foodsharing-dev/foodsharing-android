package de.foodsharing.model

import java.util.Date

data class Notification(
    val id: Int,
    val key: String,
    val title: String,
    val href: String,
    val icon: String?,
    val image: String?,
    val createdAt: Date,
    val isCloseable: Boolean,
    val isRead: Boolean,
    val payload: Map<String, String>
) {
    @Deprecated(
        "This is using the translation key, which is reliable. The backend needs to return the bell type or an" +
            " identifier instead. Then we can use NotificationType.identifierPrefix"
    )
    fun getType() = NotificationType.values().find { b -> key.startsWith(b.titleKey) } ?: NotificationType.UNKNOWN
}
