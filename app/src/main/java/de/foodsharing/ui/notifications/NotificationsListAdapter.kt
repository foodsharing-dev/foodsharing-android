package de.foodsharing.ui.notifications

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import de.foodsharing.R
import de.foodsharing.databinding.ItemNotificationBinding
import de.foodsharing.model.Notification
import de.foodsharing.model.NotificationType

class NotificationsListAdapter(
    private val onClickListener: (Notification) -> Unit,
    val context: Context
) : RecyclerView.Adapter<NotificationsListAdapter.NotificationHolder>() {
    private var notifications: List<Notification> = emptyList()

    fun setNotifications(newNotifications: List<Notification>) {
        notifications = newNotifications
    }

    override fun getItemCount(): Int = notifications.size

    override fun onBindViewHolder(holder: NotificationHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(notifications[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): NotificationHolder {
        val binding = ItemNotificationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotificationHolder(
            binding,
            onClickListener,
            context
        )
    }

    class NotificationHolder(
        val binding: ItemNotificationBinding,
        private val onClickListener: (Notification) -> Unit,
        val context: Context
    ) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private var notification: Notification? = null

        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            notification?.let {
                onClickListener(it)
            }
        }

        fun bind(notification: Notification) {
            this.notification = notification

            getTitle(notification)?.let {
                binding.itemTitleText.text = context.getString(it.first, *it.second)
            }
            getSubtitle(notification)?.let {
                binding.itemSubtitleText.text = it
            }

            binding.root.setBackgroundColor(
                ContextCompat.getColor(context, R.color.white)
            )
        }

        private fun getTitle(notification: Notification): Pair<Int, Array<String?>>? = when (notification.getType()) {
            NotificationType.BUDDY_REQUEST -> Pair(R.string.notification_title_buddy_request, emptyArray())
            NotificationType.NEW_FOOD_SHARE_POINT -> Pair(
                R.string.notification_title_new_fsp,
                arrayOf(notification.payload["bezirk"])
            )
            NotificationType.FOOD_SHARE_POINT_POST -> Pair(R.string.notification_title_fsp_post, emptyArray())
            NotificationType.NEW_FORUM_POST -> Pair(
                R.string.notification_title_new_forum_post,
                arrayOf(notification.payload["forum"])
            )
            NotificationType.NEW_FOODSAVER_IN_REGION -> Pair(
                R.string.notification_title_new_foodsaver_in_region,
                arrayOf(notification.payload["name"], notification.payload["bezirk"])
            )
            NotificationType.FOODSAVER_VERIFIED -> Pair(R.string.notification_title_foodsaver_verified, emptyArray())
            NotificationType.PASS_CREATION_FAILED -> Pair(
                R.string.notification_title_pass_creation_failed,
                emptyArray()
            )
            NotificationType.NEW_STORE_REQUEST -> Pair(R.string.notification_title_new_store_request, emptyArray())
            NotificationType.STORE_REQUEST_ACCEPTED -> Pair(
                R.string.notification_title_store_request_accepted,
                emptyArray()
            )
            NotificationType.STORE_REQUEST_REJECTED -> Pair(
                R.string.notification_title_store_request_rejected,
                emptyArray()
            )
            NotificationType.STORE_REQUEST_WAITING -> Pair(
                R.string.notification_title_store_request_waiting,
                emptyArray()
            )
            NotificationType.STORE_UNCONFIRMED_PICKUP -> Pair(
                R.string.notification_title_store_unconfirmed_pickup,
                emptyArray()
            )
            NotificationType.NEW_STORE -> Pair(R.string.notification_title_new_store, emptyArray())
            NotificationType.STORE_TIME_CHANGED -> Pair(R.string.notification_title_store_time_changed, emptyArray())
            NotificationType.STORE_WALL_POST -> Pair(R.string.notification_title_store_wall_post, emptyArray())
            NotificationType.NEW_BLOG_POST -> Pair(R.string.notification_title_new_blog_post, emptyArray())
            NotificationType.NEW_POLL -> Pair(R.string.notification_title_new_poll, emptyArray())
            NotificationType.WORKING_GROUP_REQUEST_ACCEPTED -> Pair(
                R.string.notification_title_working_group_request_accepted,
                emptyArray()
            )
            NotificationType.WORKING_GROUP_REQUEST_DENIED -> Pair(
                R.string.notification_title_working_group_request_denied,
                emptyArray()
            )
            NotificationType.NEW_REPORT -> Pair(R.string.notification_title_new_report, emptyArray())
            else -> null
        }

        private fun getSubtitle(notification: Notification): String? = when (notification.getType()) {
            NotificationType.BUDDY_REQUEST,
            NotificationType.NEW_FOOD_SHARE_POINT,
            NotificationType.NEW_FORUM_POST,
            NotificationType.NEW_FOODSAVER_IN_REGION,
            NotificationType.FOODSAVER_VERIFIED -> null
            NotificationType.PASS_CREATION_FAILED -> context.getString(
                R.string.notification_subtitle_pass_creation_failed
            )
            NotificationType.FOOD_SHARE_POINT_POST,
            NotificationType.NEW_STORE_REQUEST,
            NotificationType.STORE_REQUEST_ACCEPTED,
            NotificationType.STORE_REQUEST_REJECTED,
            NotificationType.STORE_REQUEST_WAITING,
            NotificationType.STORE_UNCONFIRMED_PICKUP,
            NotificationType.NEW_STORE,
            NotificationType.STORE_TIME_CHANGED,
            NotificationType.WORKING_GROUP_REQUEST_ACCEPTED,
            NotificationType.WORKING_GROUP_REQUEST_DENIED,
            NotificationType.STORE_WALL_POST -> notification.payload["name"]
            NotificationType.NEW_BLOG_POST,
            NotificationType.NEW_POLL -> notification.payload["title"]
            NotificationType.NEW_REPORT -> null
            else -> null
        }
    }
}
