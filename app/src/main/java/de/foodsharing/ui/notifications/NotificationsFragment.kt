package de.foodsharing.ui.notifications

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.foodsharing.R
import de.foodsharing.databinding.FragmentNotificationsBinding
import de.foodsharing.di.Injectable
import de.foodsharing.model.Notification
import de.foodsharing.model.NotificationType
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.fsp.FoodSharePointActivity
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.NOTIFICATIONS_PER_REQUEST
import de.foodsharing.utils.Utils
import javax.inject.Inject

class NotificationsFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: NotificationsViewModel by viewModels { viewModelFactory }
    private lateinit var adapter: NotificationsListAdapter
    private var _binding: FragmentNotificationsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)

        val layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.layoutManager = layoutManager
        adapter = NotificationsListAdapter({ onClickNotification(it) }, requireContext())
        binding.recyclerView.adapter = adapter

        binding.notificationsFragmentPullRefresh.setOnRefreshListener {
            viewModel.refresh()
        }

        // listener that notices if the user scrolled to the bottom of the conversations list
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val linLayoutManager = binding.recyclerView.layoutManager as? LinearLayoutManager
                val remainingItems = adapter.itemCount - (linLayoutManager?.findLastVisibleItemPosition() ?: -1)
                if (remainingItems < NOTIFICATIONS_PER_REQUEST) {
                    viewModel.loadNext()
                }
            }
        })

        binding.noNotificationsLabel.movementMethod = LinkMovementMethod.getInstance()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindViewModel()
    }

    private fun bindViewModel() {
        val diff = AsyncListDiffer(
            adapter,
            object : DiffUtil.ItemCallback<Notification>() {
                override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: Notification,
                    newItem: Notification
                ): Boolean {
                    return oldItem == newItem
                }
            }
        )

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                val linearLayoutManager = binding.recyclerView.layoutManager as? LinearLayoutManager
                val firstPosition = linearLayoutManager?.findFirstVisibleItemPosition()
                if (positionStart == 0 && firstPosition == 0) {
                    linearLayoutManager.scrollToPositionWithOffset(0, 0)
                }
            }

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                val linearLayoutManager = binding.recyclerView.layoutManager as? LinearLayoutManager
                val firstPosition = linearLayoutManager?.findFirstVisibleItemPosition()
                if ((toPosition == 0 || fromPosition == 0) && firstPosition == 0) {
                    linearLayoutManager.scrollToPositionWithOffset(0, 0)
                }
            }
        })

        viewModel.notifications.observe(
            viewLifecycleOwner,
            Observer {
                val notifications = it ?: return@Observer

                diff.submitList(notifications) {
                    adapter.setNotifications(diff.currentList)
                }

                binding.noNotificationsLabel.visibility = if (notifications.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                binding.recyclerView.visibility = if (notifications.isEmpty()) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
            }
        )

        viewModel.isLoading.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        }

        viewModel.isReloading.observe(viewLifecycleOwner) {
            binding.notificationsFragmentPullRefresh.isRefreshing = it
        }

        viewModel.showError.observe(viewLifecycleOwner) {
            showMessage(getString(it))
        }
    }

    private fun onClickNotification(notification: Notification) {
        when (notification.getType()) {
            NotificationType.FOOD_SHARE_POINT_POST, NotificationType.NEW_FOOD_SHARE_POINT -> {
                extractFSPIdFromUrl(notification.href)?.let {
                    FoodSharePointActivity.start(requireContext(), it)
                }
            }
            NotificationType.BUDDY_REQUEST -> {
                extractProfileIdFromUrl(notification.href)?.let {
                    ProfileActivity.start(requireContext(), it)
                }
            }
            else -> {
                Utils.showQuestionDialog(
                    requireContext(),
                    getString(R.string.notification_open_website_question)
                ) { result ->
                    if (result) Utils.openUrlInBrowser(requireContext(), BASE_URL + notification.href)
                }
            }
        }
    }

    @Deprecated("Extracting the ID from a notification's URL is not a good way. The backend needs to add the ID")
    private fun extractFSPIdFromUrl(url: String): Int? {
        val idx = url.indexOf("id=")
        if (idx < 0) {
            return null
        }

        val end = if (idx + 3 == url.length) {
            url.length
        } else {
            url.indexOf("&", idx + 1)
        }
        return url.substring(idx + 3, end).toInt()
    }

    @Deprecated("Extracting the ID from a notification's URL is not a good way. The backend needs to add the ID")
    private fun extractProfileIdFromUrl(url: String): Int? {
        val idx = url.lastIndexOf("/")
        return if (idx >= 0 && idx < url.length) url.substring(idx + 1).toInt() else null
    }
}
