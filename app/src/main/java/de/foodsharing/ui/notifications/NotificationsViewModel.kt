package de.foodsharing.ui.notifications

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.Notification
import de.foodsharing.services.AuthService
import de.foodsharing.services.NotificationsService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.utils.captureException
import de.foodsharing.utils.combineWith
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class NotificationsViewModel @Inject constructor(
    notificationsService: NotificationsService,
    val authService: AuthService
) : BaseViewModel() {

    private val loadNextEvents = PublishSubject.create<Any>()
    private val reloadEvents = PublishSubject.create<Any>()
    private val tryAgainEvents = PublishSubject.create<Any>()

    val notifications = MutableLiveData<List<Notification>>()

    private val isLoadingNotifications = MutableLiveData<Boolean>()
    val isReloading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Int>()
    val isLoading = isLoadingNotifications
        .combineWith(showError) { loading, errorState ->
            loading?.and(errorState == null) ?: false
        }

    init {
        isLoadingNotifications.value = true
        isReloading.value = false
        request(
            notificationsService.listPaged(
                reloadEvents.doOnNext {
                    isReloading.postValue(true)
                },
                loadNextEvents.doOnNext {
                    isLoadingNotifications.postValue(true)
                },
                getErrorHandler(isLoadingNotifications)
            ),
            {
                notifications.postValue(it)

                isLoadingNotifications.postValue(false)
                isReloading.postValue(false)
            },
            {
                // Expected errors should be handled by the error handler
                captureException(Exception("Unexpected error while loading notifications.", it))
            }
        )
    }

    private fun getErrorHandler(loadingIndicator: MutableLiveData<Boolean>):
        ((Observable<Throwable>) -> ObservableSource<Any>) = {
        it.flatMap { error ->
            if (error is IOException) {
                // Network Error
                showError.postValue(R.string.error_no_connection)
            } else if (error is HttpException && error.code() == 401) {
                showError.postValue(R.string.error_no_connection)
            } else {
                // HttpException or unknown exception
                // Should not happen
                showError.postValue(R.string.error_unknown)
                captureException(Exception("Unexpected exception in NotificationsViewModel error handler", error))
            }
            loadingIndicator.postValue(false)
            isReloading.postValue(false)
            tryAgainEvents.doOnNext {
                loadingIndicator.postValue(true)
            }
        }
    }

    fun loadNext() {
        loadNextEvents.onNext(true)
    }

    fun refresh() {
        reloadEvents.onNext(true)
    }

    fun tryAgain() {
        tryAgainEvents.onNext(true)
    }
}
