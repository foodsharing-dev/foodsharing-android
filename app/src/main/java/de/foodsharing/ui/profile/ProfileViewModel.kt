package de.foodsharing.ui.profile

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.UserAPI
import de.foodsharing.model.User
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val profileAPI: UserAPI,
    auth: AuthService
) : BaseViewModel() {
    var userId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                isCurrentUser.value = currentUserId != null && field == currentUserId

                if (profile.value?.id != field) {
                    fetch()
                }
            }
        }

    val currentUserId = auth.currentUser?.id
    val isCurrentUser = MutableLiveData<Boolean>().apply { value = false }
    val isLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Event<Int>>()
    val profile = MutableLiveData<User?>()

    private fun fetch() {
        userId?.let { id ->
            isLoading.value = true

            request(
                profileAPI.getUser(id).doOnNext {
                    isLoading.postValue(true)
                },
                { user ->
                    isLoading.value = false
                    profile.value = user
                },
                { throwable ->
                    isLoading.value = false
                    handleError(throwable)
                }
            )
        } ?: run {
            profile.value = null
        }
    }

    private fun handleError(error: Throwable) {
        val stringRes = if (error is HttpException && error.code() == 404) {
            R.string.profile_404
        } else if (error is IOException) {
            R.string.error_no_connection
        } else {
            R.string.error_unknown
        }
        showError.value = Event(stringRes)
    }
}
