package de.foodsharing.ui.pickups

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.databinding.FragmentFuturePickupsBinding
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.pickup.PickupActivity
import de.foodsharing.utils.NonScrollingLinearLayoutManager
import javax.inject.Inject

class FuturePickupsFragment : BaseFragment() {

    private var _binding: FragmentFuturePickupsBinding? = null

    private val binding get() = _binding!!

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: PickupsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: PickupsListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFuturePickupsBinding.inflate(inflater, container, false)

        binding.recyclerView.layoutManager = NonScrollingLinearLayoutManager(activity)

        adapter = PickupsListAdapter({ pickup ->
            PickupActivity.start(requireContext(), pickup.store, pickup.date)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }, requireContext())
        binding.recyclerView.adapter = adapter

        bindViewModel()

        return binding.root
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                binding.noFuturePickupsLabel.visibility = View.GONE
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.noFuturePickupsLabel.visibility = View.VISIBLE
                binding.progressBar.visibility = View.GONE
            }
        }

        viewModel.showError.observe(
            viewLifecycleOwner,
            EventObserver {
                showMessage(getString(it))
            }
        )

        viewModel.pickups.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.recyclerView.visibility = View.GONE
                binding.noFuturePickupsLabel.visibility = View.VISIBLE
            } else {
                binding.recyclerView.visibility = View.VISIBLE
                binding.noFuturePickupsLabel.visibility = View.GONE

                adapter.setPickups(it)
            }
        }
    }

    fun reload() {
        viewModel.reload()
    }
}
