package de.foodsharing.ui.pickups

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.foodsharing.databinding.ItemFuturePickupBinding
import de.foodsharing.model.UserPickup

class PickupsListAdapter(
    private val onClickListener: (UserPickup) -> Unit,
    val context: Context
) : RecyclerView.Adapter<PickupsListAdapter.PickupHolder>() {
    private var pickups = emptyList<UserPickup>()
    var dateFormatter = FuturePickupDateFormatter(context)

    override fun getItemCount() = pickups.size

    override fun onBindViewHolder(holder: PickupHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(pickups[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): PickupHolder {
        val binding = ItemFuturePickupBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PickupHolder(binding, onClickListener, context)
    }

    fun setPickups(pickups: List<UserPickup>) {
        this.pickups = pickups
        notifyDataSetChanged()
    }

    inner class PickupHolder(
        private val binding: ItemFuturePickupBinding,
        private val onClickListener: (UserPickup) -> Unit,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(pickup: UserPickup) {
            binding.root.setOnClickListener {
                onClickListener(pickup)
            }

            binding.itemDate.text = dateFormatter.format(pickup.date)
            binding.itemStoreName.text = pickup.store.name
        }
    }
}
