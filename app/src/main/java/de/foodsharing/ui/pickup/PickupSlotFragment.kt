package de.foodsharing.ui.pickup

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import de.foodsharing.R
import de.foodsharing.databinding.FragmentPickupSlotBinding
import de.foodsharing.di.Injectable
import de.foodsharing.model.StorePickupSlot
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.conversation.ConversationActivity

class PickupSlotFragment : BaseFragment(), Injectable {

    private var _binding: FragmentPickupSlotBinding? = null

    private val binding get() = _binding!!

    companion object {
        const val EXTRA_SLOT = "slot"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentPickupSlotBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (arguments?.getSerializable(EXTRA_SLOT) as? StorePickupSlot)?.let {
            setSlot(view, it)
        }
    }

    private fun setSlot(view: View, pickupSlot: StorePickupSlot) {
        binding.userName.text = pickupSlot.profile.name
        binding.buttonUserChat.setOnClickListener {
            ConversationActivity.start(requireContext(), pickupSlot.profile.id)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }

        if (!pickupSlot.profile.landline.isNullOrBlank() || !pickupSlot.profile.mobile.isNullOrBlank()) {
            binding.buttonUserCall.visibility = View.VISIBLE

            if (!pickupSlot.profile.landline.isNullOrBlank() && !pickupSlot.profile.mobile.isNullOrBlank()) {
                // two available numbers: show a popup menu
                val contextThemeWrapper = ContextThemeWrapper(context, R.style.PopupMenuOverlapAnchor)
                val popup = PopupMenu(contextThemeWrapper, binding.buttonUserCall, Gravity.TOP or Gravity.END)
                popup.menuInflater.inflate(R.menu.pickup_slot_call_menu, popup.menu)

                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.pickup_slot_call_mobile_item -> startPhoneCall(pickupSlot.profile.mobile)
                        R.id.pickup_slot_call_landline_item -> startPhoneCall(pickupSlot.profile.landline)
                    }
                    false
                }

                binding.buttonUserCall.setOnClickListener {
                    context?.let {
                        popup.show()
                    }
                }
            } else {
                // only one number: start the dial intent directly
                val number =
                    if (pickupSlot.profile.landline.isNullOrBlank()) {
                        pickupSlot.profile.landline
                    } else {
                        pickupSlot.profile.mobile
                    }
                number?.let { n ->
                    binding.buttonUserCall.setOnClickListener {
                        startPhoneCall(n)
                    }
                }
            }
        } else {
            binding.buttonUserCall.visibility = View.INVISIBLE
        }
    }

    /**
     * Starts the dial intent for a phone call.
     */
    private fun startPhoneCall(number: String) {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$number"))
        startActivity(intent)
    }
}
