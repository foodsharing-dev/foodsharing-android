package de.foodsharing.ui.content

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.api.ContentAPI
import de.foodsharing.databinding.ActivityContentEntryBinding
import de.foodsharing.di.Injectable
import de.foodsharing.model.ContentEntry
import de.foodsharing.ui.base.AuthRequiredBaseActivity
import de.foodsharing.ui.base.EventObserver
import io.noties.markwon.Markwon
import java.lang.IllegalArgumentException
import javax.inject.Inject

class ContentEntryActivity : AuthRequiredBaseActivity(), Injectable {

    companion object {
        private const val EXTRA_CONTENT_ID = "id"

        fun start(context: Context, contentID: ContentAPI.ContentID) {
            val extras = bundleOf(EXTRA_CONTENT_ID to contentID.name)
            val intent = Intent(context, ContentEntryActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityContentEntryBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val contentEntryViewModel: ContentEntryViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContentEntryBinding.inflate(layoutInflater)
        rootLayoutID = binding.root.id
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindViewModel()

        supportActionBar?.title = getString(R.string.activity_content_title)
        contentEntryViewModel.contentId = null

        val id = intent.getStringExtra(EXTRA_CONTENT_ID)
        id?.let {
            try {
                val contentId = ContentAPI.ContentID.valueOf(it)
                supportActionBar?.title = "${getString(R.string.activity_content_title)}: #${contentId.value}"
                contentEntryViewModel.contentId = contentId
            } catch (_: IllegalArgumentException) {
            }
        }
    }

    private fun bindViewModel() {
        contentEntryViewModel.isLoading.observe(this) {
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        }

        contentEntryViewModel.showError.observe(
            this,
            EventObserver {
                showErrorMessage(getString(it))
            }
        )

        contentEntryViewModel.content.observe(this) {
            display(it)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressedDispatcher.onBackPressed()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    private fun display(content: ContentEntry?) {
        content?.let {
            supportActionBar?.title = it.title
        }

        content?.body?.let { body ->
            when (contentEntryViewModel.contentId?.mode) {
                ContentAPI.ContentMode.HTML -> {
                    binding.contentEntryBody.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Html.fromHtml(body, Html.FROM_HTML_MODE_COMPACT)
                    } else {
                        Html.fromHtml(body)
                    }
                }

                ContentAPI.ContentMode.MARKDOWN -> {
                    Markwon.create(this).setMarkdown(binding.contentEntryBody, body)
                }

                else -> {}
            }
        }

        binding.contentEntryView.visibility = View.VISIBLE
        binding.progressBar.visibility = View.GONE
    }

    private fun showErrorMessage(error: String) {
        binding.progressBar.visibility = View.GONE
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }
}
