package de.foodsharing.ui.content

import android.util.Log
import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.ContentAPI
import de.foodsharing.model.ContentEntry
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.LOG_TAG
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class ContentEntryViewModel @Inject constructor(
    private val contentAPI: ContentAPI
) : BaseViewModel() {
    var contentId: ContentAPI.ContentID? = null
        set(value) {
            if (field != value) {
                field = value
                fetch()
            }
        }
    val isLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Event<Int>>()
    val content = MutableLiveData<ContentEntry?>()

    private fun fetch() {
        contentId?.let { id ->
            isLoading.value = true

            request(
                contentAPI.get(id.value).doOnNext {
                    isLoading.postValue(true)
                },
                { c ->
                    isLoading.value = false
                    content.value = c
                },
                { throwable ->
                    isLoading.value = false
                    handleError(throwable)
                }
            )
        } ?: run {
            content.value = null
        }
    }

    private fun handleError(error: Throwable) {
        Log.e(LOG_TAG, "", error)
        val stringRes = if (error is HttpException && error.code() == 404) {
            R.string.content_404
        } else if (error is IOException) {
            R.string.error_no_connection
        } else {
            R.string.error_unknown
        }
        showError.value = Event(stringRes)
    }
}
