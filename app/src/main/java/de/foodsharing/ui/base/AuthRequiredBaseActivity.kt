package de.foodsharing.ui.base

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.login.LoginActivity.Companion.EXTRA_GO_BACK

@SuppressLint("Registered")
open class AuthRequiredBaseActivity : BaseActivity() {

    private var loginLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result: ActivityResult ->
        if (result.resultCode == RESULT_CANCELED) {
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // starts login activity if not logged in
        if (!preferences.isLoggedIn) {
            val intent = Intent(this, LoginActivity::class.java).apply {
                putExtra(EXTRA_GO_BACK, true)
            }
            loginLauncher.launch(intent)
        }
    }
}
