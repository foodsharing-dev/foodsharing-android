package de.foodsharing.ui.base

import android.app.Activity
import android.content.Context
import android.widget.SeekBar
import androidx.appcompat.app.AlertDialog
import de.foodsharing.R
import de.foodsharing.databinding.LocationFilterComponentBinding

class LocationFilterComponent {

    enum class LocationType {
        HOME, CURRENT_LOCATION
    }

    companion object {
        fun showDialog(
            activity: Activity,
            locationType: LocationType,
            distance: Int,
            result: (LocationType, Int) -> Unit
        ) {
            val binding = LocationFilterComponentBinding.inflate(activity.layoutInflater)

            // add listeners
            binding.distanceSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    binding.distanceValueLabel.text = "%d km".format(progress + 1)
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }
            })
            binding.locationTypeRadioGroup.setOnCheckedChangeListener { _, checked ->
                updateLayout(activity, checked, binding)
            }

            // set current values
            binding.locationTypeRadioGroup.check(typeToRadioButton(locationType))
            binding.distanceSeekBar.progress = distance - 1
            binding.distanceValueLabel.text = "%d km".format(distance)
            updateLayout(activity, typeToRadioButton(locationType), binding)

            // create and show the dialog
            val builder = AlertDialog.Builder(activity)
            builder.setTitle(activity.getString(R.string.location_dialog_title))
                .setView(binding.root)
                .setPositiveButton(android.R.string.ok) { dialog, id ->
                    val newDistance = binding.distanceSeekBar.progress + 1
                    val newType = radioButtonToType(binding.locationTypeRadioGroup.checkedRadioButtonId)
                    result(newType, newDistance)
                    dialog.dismiss()
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    dialog.dismiss()
                }
            builder.create().show()
        }

        private fun typeToRadioButton(type: LocationType) = when (type) {
            LocationType.CURRENT_LOCATION -> R.id.location_type_radio_current
            else -> R.id.location_type_radio_home
        }

        private fun radioButtonToType(id: Int) = when (id) {
            R.id.location_type_radio_current -> LocationType.CURRENT_LOCATION
            else -> LocationType.HOME
        }

        private fun updateLayout(context: Context, checkedType: Int, binding: LocationFilterComponentBinding) {
            val text: String
            val progressEnabled: Boolean
            when (checkedType) {
                R.id.location_type_radio_current -> {
                    text = context.getString(R.string.location_dialog_location_type_current)
                    progressEnabled = true
                }

                else -> {
                    text = context.getString(R.string.location_dialog_location_type_home)
                    progressEnabled = false
                }
            }
            binding.locationTypeText.text = text
            binding.distanceSeekBar.isEnabled = progressEnabled
            binding.distanceLabel.isEnabled = progressEnabled
            binding.distanceValueLabel.isEnabled = progressEnabled
        }
    }
}
