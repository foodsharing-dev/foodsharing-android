package de.foodsharing.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Point
import android.os.Bundle
import android.provider.Settings
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.databinding.FragmentMapBinding
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.CommunityMarker
import de.foodsharing.model.Coordinate
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.community.CommunityActivity
import de.foodsharing.ui.fsp.FoodSharePointActivity
import de.foodsharing.utils.DEFAULT_MAP_ZOOM
import de.foodsharing.utils.LocationFinder
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.OsmdroidUtils.toCoordinate
import de.foodsharing.utils.OsmdroidUtils.toGeoPoint
import de.foodsharing.utils.Utils
import de.foodsharing.utils.Utils.getBasketMarkerIcon
import de.foodsharing.utils.Utils.getCommunityMarkerIcon
import de.foodsharing.utils.Utils.getFspMarkerIcon
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.mylocation.SimpleLocationOverlay
import javax.inject.Inject

class MapFragment : BaseFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var _binding: FragmentMapBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MapViewModel by viewModels { viewModelFactory }

    private lateinit var markerOverlay: RadiusMarkerClusterer
    private var basketMarkers: List<Marker>? = null
    private var fspMarkers: List<Marker>? = null
    private var communityMarkers: List<Marker>? = null
    private val mapView: MapView by lazy {
        binding.map
    }
    private lateinit var locationOverlay: SimpleLocationOverlay
    private var locationCallbackCancelable: (() -> Unit)? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = FragmentMapBinding.inflate(inflater, container, false)

        if (savedInstanceState != null) {
            savedInstanceState.getParcelable<Coordinate>(STATE_COORDINATE)?.let {
                val zoom = savedInstanceState.getDouble(STATE_ZOOM)
                updateLocation(it, zoom, animate = false)
            }
        }

        if (!canRestoreMapCamera(savedInstanceState)) {
            // try to restore old camera view or zoom out to germany
            if (!restoreMapCamera()) {
                val initialBoundingBox = BoundingBox.fromGeoPoints(
                    listOf(GeoPoint(46.0, 4.0), GeoPoint(55.0, 17.0))
                )
                mapView.addOnFirstLayoutListener { _, _, _, _, _ ->
                    mapView.zoomToBoundingBox(initialBoundingBox, false)
                }
            }
        }

        OsmdroidUtils.setupMapView(mapView, preferences.allowHighResolutionMap)

        context?.let {
            setupMarkerOverlay(it)
            setupLocationOverlay(it)
        }

        setupLayersButton()

        binding.mapLocationButton.setOnClickListener {
            onFindLocationClick()
        }

        mapView.setOnTouchListener { _, _ ->
            stopLocationUpdates()
            false
        }

        bindViewModel()

        return binding.root
    }

    /**
     * Sets the location and zoom of the map.
     */
    fun updateLocation(point: Coordinate, zoom: Double? = null, animate: Boolean? = null) {
        updateLocation(point.toGeoPoint(), zoom, animate)
    }

    fun getMapCenter(): Coordinate {
        return mapView.mapCenter.toCoordinate()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(STATE_COORDINATE, mapView.mapCenter.toCoordinate())
        outState.putDouble(STATE_ZOOM, mapView.zoomLevelDouble)
        super.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        storeMapCamera()
        stopLocationUpdates()
        mapView.onDetach()
    }

    private fun setupLocationOverlay(it: Context) {
        val bitmap = Utils.getBitmapFromResourceId(it, R.drawable.location_circle) ?: return
        locationOverlay = SimpleLocationOverlay(bitmap)
        locationOverlay.setLocation(null)
        locationOverlay.setPersonIcon(bitmap, Point(bitmap.width / 2, bitmap.height / 2))
        mapView.overlays.add(locationOverlay)
    }

    private fun setupMarkerOverlay(it: Context) {
        markerOverlay = FsRadiusMarkerClusterer(it).apply {
            onClusterClickListener = this@MapFragment::onClusterClick
        }
        markerOverlay.setRadius((it.resources.displayMetrics.density * 72).toInt())
        markerOverlay.setMaxClusteringZoomLevel(Integer.MAX_VALUE)

        mapView.overlays.add(markerOverlay)
    }

    private val permissionRequestLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            if (granted) {
                context?.applicationContext?.let {
                    LocationFinder.instance.initialise(it)
                    findLocation()
                }
            }
        }

    private fun setupLayersButton() {
        val layersButton = binding.mapLayersButton

        val contextThemeWrapper = ContextThemeWrapper(context, R.style.PopupMenuOverlapAnchor)
        val popup = PopupMenu(contextThemeWrapper, layersButton, Gravity.TOP or Gravity.END)
        popup.menuInflater.inflate(R.menu.layer_menu, popup.menu)

        popup.menu.findItem(R.id.basket_layer_item).isChecked = viewModel.isShowingBaskets.value == true
        popup.menu.findItem(R.id.fsp_layer_item).isChecked = viewModel.isShowingFSPs.value == true
        popup.menu.findItem(R.id.community_layer_item).isChecked = viewModel.isShowingCommunities.value == true

        popup.setOnMenuItemClickListener { item ->
            item.isChecked = !item.isChecked
            when (item.itemId) {
                R.id.basket_layer_item -> viewModel.isShowingBaskets.value = item.isChecked
                R.id.fsp_layer_item -> viewModel.isShowingFSPs.value = item.isChecked
                R.id.community_layer_item -> viewModel.isShowingCommunities.value = item.isChecked
            }
            updateMarkers()
            false
        }

        layersButton.setOnClickListener {
            context?.let {
                popup.show()
            }
        }
    }

    private fun bindViewModel() {
        viewModel.showError.observe(
            viewLifecycleOwner,
            EventObserver {
                showMessage(getString(it))
            }
        )

        viewModel.markers.observe(viewLifecycleOwner) {
            setMarkers(
                it?.first ?: emptyList(),
                it?.second ?: emptyList(),
                it?.third ?: emptyList()
            )
        }

        viewModel.isShowingBaskets.observe(viewLifecycleOwner) {
            updateMarkers()
        }
        viewModel.isShowingFSPs.observe(viewLifecycleOwner) {
            updateMarkers()
        }
        viewModel.isShowingCommunities.observe(viewLifecycleOwner) {
            updateMarkers()
        }
    }

    private fun restoreMapCamera(): Boolean {
        updateLocation(
            preferences.mapCenterCoordinate ?: return false,
            preferences.mapZoom,
            false
        )
        return true
    }

    private fun storeMapCamera() {
        preferences.mapCenterCoordinate = Coordinate(
            mapView.boundingBox.centerLatitude,
            mapView.boundingBox.centerLongitude
        )
        preferences.mapZoom = mapView.zoomLevelDouble
    }

    private fun setMarkers(
        foodSharePoints: List<FoodSharePoint>,
        baskets: List<Basket>,
        communities: List<CommunityMarker>
    ) {
        // add food-share-point markers
        val fspIcon = getFspMarkerIcon(requireContext())
        fspMarkers = foodSharePoints.map { fsp ->
            val marker = Marker(mapView)
            marker.id = fsp.id.toString()
            marker.position = GeoPoint(fsp.lat, fsp.lon)
            marker.icon = fspIcon
            marker.relatedObject = fsp
            marker.setOnMarkerClickListener { m, _ ->
                val foodsp = m.relatedObject as FoodSharePoint

                // show fsp details in an activity
                FoodSharePointActivity.start(requireContext(), foodsp.id)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }

        val basketIcon = getBasketMarkerIcon(requireContext())
        basketMarkers = baskets.map { b ->
            val marker = Marker(mapView)
            marker.id = b.id.toString()
            marker.position = GeoPoint(b.lat, b.lon)
            marker.icon = basketIcon
            marker.relatedObject = b
            marker.setOnMarkerClickListener { m, _ ->
                val basket = m.relatedObject as Basket

                // show basket details in an activity
                BasketActivity.start(requireContext(), basket.id)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }

        val communityIcon = getCommunityMarkerIcon(requireContext())
        communityMarkers = communities.map { c ->
            val marker = Marker(mapView)
            marker.id = c.id.toString()
            marker.position = GeoPoint(c.lat, c.lon)
            marker.icon = communityIcon
            marker.relatedObject = c
            marker.setOnMarkerClickListener { m, _ ->
                val community = m.relatedObject as CommunityMarker

                // show community description in an activity
                CommunityActivity.start(requireContext(), community.id)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }

        updateMarkers()
    }

    private fun onClusterClick(markers: List<Marker>) {
        val ctx = context ?: return
        val builder = AlertDialog.Builder(ctx)

        val arrayAdapter = ArrayAdapter<String>(ctx, android.R.layout.select_dialog_item)
        markers.map {
            when (val relatedObj = it.relatedObject) {
                is Basket -> "${getString(R.string.basket_label)} #${relatedObj.id}"
                is FoodSharePoint -> "${getString(R.string.foodsharepoint_label)} #${relatedObj.id}"
                is CommunityMarker -> "${getString(R.string.community_label)} #${relatedObj.id}"
                else -> getString(R.string.unknown)
            }
        }.forEach { arrayAdapter.add(it) }

        builder.setNegativeButton(android.R.string.cancel, null)

        builder.setAdapter(arrayAdapter) { _, index ->
            when (val relatedObj = markers[index].relatedObject) {
                is Basket -> BasketActivity.start(requireContext(), relatedObj.id)
                is FoodSharePoint -> FoodSharePointActivity.start(requireContext(), relatedObj.id)
                // is CommunityMarker -> CommunityActivity.start(requireContext(), relatedObj.regionId)
                else -> return@setAdapter
            }
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }
        builder.show()
    }

    private fun updateMarkers() {
        markerOverlay.items.clear()

        if (viewModel.isShowingBaskets.value == true) {
            basketMarkers?.forEach {
                markerOverlay.add(it)
            }
        }
        if (viewModel.isShowingFSPs.value == true) {
            fspMarkers?.forEach {
                markerOverlay.add(it)
            }
        }
        if (viewModel.isShowingCommunities.value == true) {
            communityMarkers?.forEach {
                markerOverlay.add(it)
            }
        }

        markerOverlay.invalidate()
        mapView.invalidate()
    }

    private fun findLocation() {
        if (LocationFinder.instance.isLocationAvailable()) {
            startLocationUpdates()
        } else {
            showLocationNotification()
        }
    }

    private fun startLocationUpdates() {
        locationCallbackCancelable?.let { it() }

        val callback = LocationFinder.instance.requestLocation {
            val point = GeoPoint(it)
            updateLocation(point)
            locationOverlay.setLocation(point)
        }
        binding.mapLocationButton.supportImageTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorSecondary))
        locationCallbackCancelable = {
            binding.mapLocationButton.supportImageTintList = null
            callback()
        }
    }

    private fun stopLocationUpdates() {
        locationCallbackCancelable?.let { it() }
    }

    private fun onFindLocationClick() {
        val permission = Manifest.permission.ACCESS_FINE_LOCATION
        if (context?.let { ContextCompat.checkSelfPermission(it, permission) } == PackageManager.PERMISSION_GRANTED) {
            findLocation()
        } else {
            // request real-time permissions (api >= 23)
            permissionRequestLauncher.launch(permission)
        }
    }

    private fun updateLocation(point: GeoPoint, zoom: Double? = null, animate: Boolean? = null) {
        val requestedZoom = zoom ?: DEFAULT_MAP_ZOOM
        val locationZoom = if (mapView.zoomLevelDouble > requestedZoom) {
            mapView.zoomLevelDouble
        } else {
            requestedZoom
        }
        val locationAnimate = animate ?: true
        if (locationAnimate) {
            mapView.controller.animateTo(point, locationZoom, null)
        } else {
            mapView.controller.setZoom(locationZoom)
            mapView.controller.setCenter(point)
        }
    }

    private fun canRestoreMapCamera(savedInstanceState: Bundle?) =
        savedInstanceState != null && savedInstanceState.containsKey(STATE_ZOOM) && savedInstanceState.containsKey(
            STATE_COORDINATE
        )

    /**
     * Notifies the user that location services are disabled.
     */
    private fun showLocationNotification() {
        AlertDialog.Builder(context ?: return).setMessage(R.string.map_location_services_not_enabled)
            .setPositiveButton(R.string.map_open_location_settings) { _, _ ->
                context?.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }.setNegativeButton(android.R.string.cancel, null).show()
    }

    companion object {
        private const val STATE_COORDINATE = "coordinate"
        private const val STATE_ZOOM = "zoom"
    }
}
