package de.foodsharing.ui.map

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.MapAPI
import de.foodsharing.model.Basket
import de.foodsharing.model.CommunityMarker
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import javax.inject.Inject

class MapViewModel @Inject constructor(
    mapAPI: MapAPI
) : BaseViewModel() {
    val showError = MutableLiveData<Event<Int>>()
    val markers = MutableLiveData<Triple<List<FoodSharePoint>, List<Basket>, List<CommunityMarker>>>()
    val isShowingFSPs = MutableLiveData<Boolean>().apply { value = true }
    val isShowingBaskets = MutableLiveData<Boolean>().apply { value = true }
    val isShowingCommunities = MutableLiveData<Boolean>().apply { value = false }

    init {
        request(mapAPI.coordinates(), { response ->
            markers.value = Triple(response.foodSharePoints, response.baskets, response.communities)
        }, {
            showError.value = Event(R.string.error_unknown)
        })
    }
}
