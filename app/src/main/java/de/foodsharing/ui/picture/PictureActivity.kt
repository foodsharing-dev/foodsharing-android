package de.foodsharing.ui.picture

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.os.bundleOf
import com.bumptech.glide.Glide
import de.foodsharing.R
import de.foodsharing.databinding.ActivityPictureBinding
import de.foodsharing.ui.base.BaseActivity

class PictureActivity : BaseActivity() {

    private lateinit var binding: ActivityPictureBinding

    companion object {
        const val EXTRA_PICTURE_FILE = "pictureFile"
        private const val EXTRA_PICTURE_URL = "pictureUrl"
        const val EXTRA_SHOW_DELETE_BUTTON = "show_delete_button"

        fun start(context: Context, pictureUrl: String) {
            val extras = bundleOf(EXTRA_PICTURE_URL to pictureUrl)
            val intent = Intent(context, PictureActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPictureBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.pictureToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        val path = if (intent.hasExtra(EXTRA_PICTURE_FILE)) {
            val filePath = intent.getStringExtra(EXTRA_PICTURE_FILE)
            "file:$filePath"
        } else if (intent.hasExtra(EXTRA_PICTURE_URL)) {
            intent.getStringExtra(EXTRA_PICTURE_URL)
        } else {
            null
        }

        path?.let {
            Glide.with(this)
                .load(path)
                .into(binding.pictureActivityView)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.picture_menu, menu)
        if (!intent.getBooleanExtra(EXTRA_SHOW_DELETE_BUTTON, false)) {
            menu.findItem(R.id.picture_button_remove)?.isVisible = false
        }
        return true
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        setResult(RESULT_OK)
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }

        R.id.picture_button_remove -> {
            setResult(RESULT_CANCELED)
            finish()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
