package de.foodsharing.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.foodsharing.R
import de.foodsharing.databinding.FragmentGiveBinding
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.baskets.MyBasketsFragment
import de.foodsharing.ui.fsp.NearbyFoodSharePointsFragment
import de.foodsharing.ui.newbasket.NewBasketActivity

class GiveFragment : BaseFragment(), Injectable {

    private var _binding: FragmentGiveBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGiveBinding.inflate(inflater, container, false)

        binding.addBasketButton.setOnClickListener {
            NewBasketActivity.start(requireContext())
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }

        if (savedInstanceState == null) {
            activity?.supportFragmentManager?.let {
                it.beginTransaction()
                    .replace(R.id.fragment_my_baskets, MyBasketsFragment(), MyBasketsFragment::class.qualifiedName)
                    .replace(
                        R.id.fragment_nearby_fsps,
                        NearbyFoodSharePointsFragment(),
                        NearbyFoodSharePointsFragment::class.qualifiedName
                    )
                    .commit()

                binding.fragmentGivePullRefresh.setOnRefreshListener {
                    (it.findFragmentByTag(MyBasketsFragment::class.qualifiedName) as? MyBasketsFragment)
                        ?.reload()
                    (
                        it.findFragmentByTag(NearbyFoodSharePointsFragment::class.qualifiedName) as?
                            NearbyFoodSharePointsFragment
                        )?.reload()
                    binding.fragmentGivePullRefresh.isRefreshing = false
                }
            }
        }

        return binding.root
    }
}
