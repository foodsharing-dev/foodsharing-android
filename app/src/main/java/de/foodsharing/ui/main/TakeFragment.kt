package de.foodsharing.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.foodsharing.R
import de.foodsharing.databinding.FragmentTakeBinding
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.baskets.NearbyBasketsFragment
import de.foodsharing.ui.pickups.FuturePickupsFragment

class TakeFragment : BaseFragment(), Injectable {

    private var _binding: FragmentTakeBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTakeBinding.inflate(inflater, container, false)

        if (savedInstanceState == null) {
            activity?.supportFragmentManager?.let {
                it.beginTransaction()
                    .replace(
                        R.id.fragment_future_pickups,
                        FuturePickupsFragment(),
                        FuturePickupsFragment::class.qualifiedName
                    )
                    .replace(
                        R.id.fragment_nearby_baskets,
                        NearbyBasketsFragment(),
                        NearbyBasketsFragment::class.qualifiedName
                    )
                    .commit()

                binding.fragmentTakePullRefresh.setOnRefreshListener {
                    (it.findFragmentByTag(FuturePickupsFragment::class.qualifiedName) as? FuturePickupsFragment)
                        ?.reload()
                    (it.findFragmentByTag(NearbyBasketsFragment::class.qualifiedName) as? NearbyBasketsFragment)
                        ?.reload()
                    binding.fragmentTakePullRefresh.isRefreshing = false
                }
            }
        }

        return binding.root
    }
}
