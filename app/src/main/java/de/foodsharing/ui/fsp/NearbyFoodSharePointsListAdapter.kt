package de.foodsharing.ui.fsp

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.foodsharing.databinding.ItemFoodsharepointBinding
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.utils.Utils
import java.util.Locale

class NearbyFoodSharePointsListAdapter(
    private val onClickListener: (FoodSharePoint) -> Unit,
    val context: Context
) : RecyclerView.Adapter<NearbyFoodSharePointsListAdapter.FoodSharePointHolder>() {
    private var foodSharePoints = emptyList<Pair<FoodSharePoint, Double>>()

    override fun getItemCount() = foodSharePoints.size

    override fun onBindViewHolder(holder: FoodSharePointHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(foodSharePoints[position].first, foodSharePoints[position].second)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): FoodSharePointHolder {
        val binding = ItemFoodsharepointBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FoodSharePointHolder(binding, onClickListener, context)
    }

    fun setFoodSharePoints(foodSharePoints: List<Pair<FoodSharePoint, Double>>) {
        this.foodSharePoints = foodSharePoints
        notifyDataSetChanged()
    }

    class FoodSharePointHolder(
        val binding: ItemFoodsharepointBinding,
        private val onClickListener: (FoodSharePoint) -> Unit,
        val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        private var foodSharePoint: FoodSharePoint? = null

        fun bind(foodSharePoint: FoodSharePoint, distance: Double) {
            binding.root.setOnClickListener {
                onClickListener(foodSharePoint)
            }
            this.foodSharePoint = foodSharePoint

            binding.itemIcon.setImageDrawable(Utils.getFspMarkerIcon(context))
            binding.itemName.text = foodSharePoint.name
            binding.itemDistance.text = Utils.formatDistance(distance).uppercase(Locale.getDefault())
        }
    }
}
