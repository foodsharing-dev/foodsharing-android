package de.foodsharing.ui.fsp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.databinding.FragmentNearbyFoodSharePointsBinding
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.base.LocationFilterComponent
import de.foodsharing.utils.NonScrollingLinearLayoutManager
import de.foodsharing.utils.UserLocation
import javax.inject.Inject

class NearbyFoodSharePointsFragment : BaseFragment() {

    private var _binding: FragmentNearbyFoodSharePointsBinding? = null

    private val binding get() = _binding!!

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var userLocation: UserLocation

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private val viewModel: NearbyFoodSharePointsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: NearbyFoodSharePointsListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNearbyFoodSharePointsBinding.inflate(inflater, container, false)

        binding.recyclerView.layoutManager = NonScrollingLinearLayoutManager(activity)

        adapter = NearbyFoodSharePointsListAdapter({ foodSharePoint ->
            FoodSharePointActivity.start(requireContext(), foodSharePoint.id)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }, requireContext())
        binding.recyclerView.adapter = adapter

        bindViewModel()

        userLocation.currentCoordinates.observe(viewLifecycleOwner) {
            it.let {
                val prevCoordinate = userLocation.previousCoordinate
                if (it.lat != 0.0 && it.lon != 0.0 && prevCoordinate.lat != it.lat && prevCoordinate.lon != it.lon) {
                    viewModel.reload()
                }
            }
        }

        binding.nearbyFspsSettingsButton.setOnClickListener {
            showSettingsDialog()
        }

        return binding.root
    }

    private fun bindViewModel() {
        viewModel.setDistance(preferenceManager.nearbyFSPsDistance)
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                binding.noFspsLabel.visibility = View.GONE
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }

        viewModel.showError.observe(
            viewLifecycleOwner,
            EventObserver {
                showMessage(getString(it))
            }
        )

        viewModel.foodSharePoints.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.recyclerView.visibility = View.GONE
                binding.noFspsLabel.visibility = View.VISIBLE
            } else {
                binding.recyclerView.visibility = View.VISIBLE
                binding.noFspsLabel.visibility = View.GONE

                adapter.setFoodSharePoints(it)
            }
        }
    }

    private fun showSettingsDialog() {
        LocationFilterComponent.showDialog(
            requireActivity(),
            preferenceManager.nearbyFSPsLocationType,
            preferenceManager.nearbyFSPsDistance
        ) { locationType, distance ->
            preferenceManager.nearbyFSPsLocationType = locationType
            preferenceManager.nearbyFSPsDistance = distance
            viewModel.setDistance(distance)
            reload()
        }
    }

    fun reload() {
        viewModel.reload()
    }
}
