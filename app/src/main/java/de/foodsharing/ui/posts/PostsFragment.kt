package de.foodsharing.ui.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import de.foodsharing.api.PostsAPI
import de.foodsharing.databinding.FragmentPostsBinding
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import javax.inject.Inject

class PostsFragment : BaseFragment(), Injectable {

    private var _binding: FragmentPostsBinding? = null

    private val binding get() = _binding!!

    private var label = ""
    private lateinit var postListAdapter: PostListAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: PostsViewModel by viewModels { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentPostsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { context ->
            postListAdapter = PostListAdapter(context, viewModel.currentUserId)
            binding.postsContainer.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = postListAdapter
            }
            bindViewModel()
        }
    }

    private fun bindViewModel() {
        viewModel.posts().observe(viewLifecycleOwner) { postList ->
            postListAdapter.setPosts(postList)
            binding.postsLabel.visibility =
                if (label == "" || (postList.isEmpty() && viewModel.mayPost().value == false)) GONE else VISIBLE
        }

        viewModel.mayPost().observe(viewLifecycleOwner) { mayPost ->
            binding.createPostWidgets.visibility = if (mayPost) VISIBLE else GONE
            binding.postsLabel.visibility =
                if (label == "" || (!mayPost && viewModel.posts().value.isNullOrEmpty())) GONE else VISIBLE
        }

        viewModel.mayDelete().observe(viewLifecycleOwner) {
            postListAdapter.mayDelete = it
        }

        viewModel.error().observe(
            viewLifecycleOwner,
            EventObserver {
                showMessage(getString(it))
            }
        )
    }

    fun setTarget(target: PostsAPI.Target, targetId: Int, label: String? = null) {
        postListAdapter.deletePost = { postId ->
            viewModel.deletePost(target, targetId, postId)
        }

        binding.sendMessageButton.setOnClickListener {
            viewModel.sendPost(target, targetId, binding.postEditText.text.toString()) {
                // on success
                binding.postEditText.setText("")
            }
        }

        label?.let {
            this.label = it
            binding.postsLabel.text = it
        }

        viewModel.fetchPosts(target, targetId)
    }
}
