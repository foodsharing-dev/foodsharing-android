package de.foodsharing.ui.posts

import android.content.Context
import android.text.format.DateUtils.FORMAT_SHOW_DATE
import android.text.format.DateUtils.FORMAT_SHOW_TIME
import android.text.format.DateUtils.FORMAT_SHOW_WEEKDAY
import android.text.format.DateUtils.formatDateTime
import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.foodsharing.R
import de.foodsharing.databinding.ItemPostBinding
import de.foodsharing.model.Post
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.utils.Utils

class PostListAdapter(
    val context: Context,
    val currentUserId: Int?
) : RecyclerView.Adapter<PostListAdapter.PostHolder>() {
    private var posts = mutableListOf<Post>()
    fun setPosts(posts: Collection<Post>) {
        this.posts = posts.toMutableList()
        notifyDataSetChanged()
    }

    var mayDelete = false
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var deletePost: ((postId: Int) -> Unit)? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = posts.size

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        if (position in 0 until itemCount) {
            posts[position].let { post ->
                holder.bind(post, mayDelete) {
                    deletePost?.invoke(post.id)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): PostHolder {
        val binding = ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostHolder(binding, context, currentUserId)
    }

    class PostHolder(
        val binding: ItemPostBinding,
        val context: Context,
        val currentUserId: Int?
    ) : RecyclerView.ViewHolder(binding.root) {

        private var deleteBoundPost: (() -> Unit)? = null
        private val pictureAdapter = PostPictureListAdapter(context)

        init {
            binding.userPicture.setCorners(15f, 15f, 15f, 15f)
            binding.postPictures.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = pictureAdapter
            }
            binding.postDeleteButton.setOnClickListener {
                AlertDialog.Builder(context)
                    .setMessage(R.string.delete_post_confirmation)
                    .setPositiveButton(R.string.delete_post) { _, _ ->
                        deleteBoundPost?.invoke()
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .show()
            }
        }

        fun bind(post: Post, mayDelete: Boolean, deleteBoundPost: () -> Unit) {
            this.deleteBoundPost = deleteBoundPost
            binding.apply {
                binding.userPicture.let {
                    Glide.with(context)
                        .load(Utils.getUserPhotoURL(post.author, Utils.PhotoType.MINI, 35))
                        .error(R.drawable.default_user_picture)
                        .fitCenter()
                        .centerCrop()
                        .into(it)
                    it.setOnClickListener {
                        ProfileActivity.start(context, post.author)
                    }
                }
                binding.postBody.text = post.body
                binding.postTimeAndAuthorName.text = context.getString(
                    R.string.created_at_by_author,
                    formatDateTime(
                        context,
                        post.createdAt.time,
                        FORMAT_SHOW_WEEKDAY or FORMAT_SHOW_DATE or FORMAT_SHOW_TIME
                    ),
                    post.author.name
                )
                binding.postDeleteButton.visibility =
                    if (mayDelete || post.author.id == currentUserId) VISIBLE else GONE
            }

            pictureAdapter.setPictures(post.pictures.orEmpty())
        }
    }
}
