package de.foodsharing.ui.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.HIDE_NOT_ALWAYS
import androidx.activity.viewModels
import androidx.core.content.getSystemService
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.databinding.ActivityLoginBinding
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.Utils
import javax.inject.Inject

class LoginActivity : BaseActivity(), Injectable {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }

        const val EXTRA_GO_BACK = "goBack"
    }

    private lateinit var binding: ActivityLoginBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val loginViewModel: LoginViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        rootLayoutID = android.R.id.content

        bindViewModel()

        binding.layoutLoginForm?.let { loginFormBinding ->
            loginFormBinding.loginButton.setOnClickListener {
                // Hide soft keyboard
                this.currentFocus?.let {
                    val inputManager = getSystemService<InputMethodManager>()!!
                    inputManager.hideSoftInputFromWindow(it.windowToken, HIDE_NOT_ALWAYS)
                }
                // Trigger login
                loginViewModel.login(
                    loginFormBinding.userField.text.toString(),
                    loginFormBinding.passwordField.text.toString()
                )
            }

            loginFormBinding.registerButton.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("$LINK_BASE_URL?page=content&sub=joininfo"))
                startActivity(browserIntent)
            }

            loginFormBinding.passwordField.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginFormBinding.loginButton.performClick()
                    return@setOnEditorActionListener true
                }
                false
            }
        }

        setupVersionText()
        setupForgotPasswordLink()
    }

    private fun bindViewModel() {
        loginViewModel.isLoading.observe(this) {
            this.showProgress(it)
        }

        loginViewModel.showError.observe(
            this,
            EventObserver {
                showMessage(getString(it))
            }
        )

        loginViewModel.loginFinished.observe(
            this,
            EventObserver {
                if (intent.getBooleanExtra(EXTRA_GO_BACK, false)) {
                    setResult(RESULT_OK)
                    finish()
                } else {
                    MainActivity.start(this)
                }
                finish()
            }
        )

        loginViewModel.popup.observe(
            this,
            EventObserver {
                Utils.handlePopup(this, it)
            }
        )
    }

    private fun setupVersionText() {
        val version =
            getString(R.string.version, BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE, BuildConfig.FLAVOR)
        val reportIssue = getString(R.string.report_login_issue)
        val versionText = SpannableString("$version - $reportIssue")
        versionText.setSpan(
            object : ClickableSpan() {
                override fun onClick(widget: View) {
                    Utils.openSupportEmail(this@LoginActivity, R.string.support_email_subject_login_suffix)
                }
            },
            versionText.length - reportIssue.length,
            versionText.length,
            0
        )
        binding.layoutLoginForm?.let { activityLoginFormBinding ->
            activityLoginFormBinding.versionTextView.movementMethod = LinkMovementMethod.getInstance()
            activityLoginFormBinding.versionTextView.text = versionText
        }
    }

    private fun setupForgotPasswordLink() {
        binding.layoutLoginForm?.let { activityLoginFormBinding ->
            val text = SpannableString(activityLoginFormBinding.forgotPasswordLink.text)
            text.setSpan(
                object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        val browserIntent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("$LINK_BASE_URL/?page=login&sub=passwordReset")
                        )
                        startActivity(browserIntent)
                    }
                },
                0,
                text.length,
                0
            )
            activityLoginFormBinding.forgotPasswordLink.movementMethod = LinkMovementMethod.getInstance()
            activityLoginFormBinding.forgotPasswordLink.text = text
        }
    }

    private fun showProgress(show: Boolean) {
        binding.progressBar.visibility = if (show) {
            View.VISIBLE
        } else {
            View.INVISIBLE
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }
}
