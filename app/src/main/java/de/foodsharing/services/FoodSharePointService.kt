package de.foodsharing.services

import de.foodsharing.api.FoodSharePointAPI
import de.foodsharing.model.FoodSharePoint
import io.reactivex.Observable
import javax.inject.Inject

class FoodSharePointService @Inject constructor(
    private val foodSharePointAPI: FoodSharePointAPI
) {
    fun listNearby(lat: Double, lon: Double, distance: Int): Observable<List<FoodSharePoint>> {
        return foodSharePointAPI.getNearby(lat, lon, distance)
    }
}
