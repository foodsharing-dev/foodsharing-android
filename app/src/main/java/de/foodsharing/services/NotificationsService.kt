package de.foodsharing.services

import de.foodsharing.api.NotificationAPI
import de.foodsharing.model.Notification
import de.foodsharing.utils.NOTIFICATIONS_PER_REQUEST
import de.foodsharing.utils.testing.OpenForTesting
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class NotificationsService @Inject constructor(
    private val notificationAPI: NotificationAPI
) {
    private val readEvents = PublishSubject.create<Notification>()
    private val removeEvents = PublishSubject.create<Notification>()

    private fun listPage(page: Int): Observable<List<Notification>> {
        val overlap = if (page > 0) 1 else 0
        return notificationAPI.list(NOTIFICATIONS_PER_REQUEST + overlap, page * NOTIFICATIONS_PER_REQUEST - overlap)
    }

    fun listPaged(
        reloadEvents: Observable<Any>,
        loadNextEvents: Observable<Any>,
        errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>)
    ): Observable<List<Notification>> {
        // Observable of the individual chunks of notifications
        val pagedNotifications = Observable.range(1, Integer.MAX_VALUE)
            .concatMap { page ->
                loadNextEvents.take(1)
                    .flatMap {
                        listPage(page).subscribeOn(Schedulers.io()).retryWhen(errorHandler)
                    }
            }.takeUntil {
                it.size < NOTIFICATIONS_PER_REQUEST + 1
            }.subscribeOn(Schedulers.io())

        // An observables of updates to the current list of notifications. Updates can either be a new page or a
        // notification which was marked as read
        val updates = Observable.merge(
            pagedNotifications.map {
                NotificationListUpdate.NewPageUpdate(it)
            },
            readEvents.map { NotificationListUpdate.MarkedAsReadUpdate(it) },
            removeEvents.map { NotificationListUpdate.RemovedUpdate(it) }
        )

        // A subject which triggers to reload the whole conversation up to a specific index
        val loadNumSubject = PublishSubject.create<Int>()

        // A mapping which applies the updates to the current list of notifications or triggers loadNumSubject if
        // this is not possible to reload the notifications
        val notificationsUpdater = { current: List<Notification>, update: NotificationListUpdate ->
            when (update) {
                is NotificationListUpdate.NewPageUpdate -> {
                    if (update.notifications.isEmpty()) {
                        current
                    } else if (current.last().id == update.notifications.first().id) {
                        current + update.notifications.subList(1, update.notifications.size)
                    } else {
                        loadNumSubject.onNext(current.size + update.notifications.size - 1)
                        current
                    }
                }
                is NotificationListUpdate.MarkedAsReadUpdate -> {
                    val newList = current.toMutableList()
                    val index = current.indexOfFirst {
                        it.id == update.notification.id
                    }
                    if (index != -1) {
                        newList[index] = current[index].copy(isRead = true)
                    }
                    newList
                }
                is NotificationListUpdate.RemovedUpdate -> {
                    val newList = current.toMutableList()
                    val index = current.indexOfFirst {
                        it.id == update.notification.id
                    }
                    if (index != -1) {
                        newList.removeAt(index)
                    }
                    newList
                }
            }.sortedWith(
                compareByDescending { it.createdAt }
            )
        }

        // Loads the initial notifications and applies subsequent updates
        val fullNotifications = loadNumSubject.startWith(NOTIFICATIONS_PER_REQUEST).switchMap { numItems ->
            notificationAPI.list(numItems, 0)
                .subscribeOn(Schedulers.io())
                .retryWhen(errorHandler)
                .switchMap { first ->
                    updates.scan(first, notificationsUpdater)
                }.switchMap { notifications ->
                    Observable.just(notifications)
                }
        }.subscribeOn(Schedulers.io())

        return Observable.just(Any()).concatWith(reloadEvents).switchMap {
            fullNotifications
        }
    }

    fun remove(notification: Notification): Observable<Unit> {
        return notificationAPI.remove(notification.id).doOnComplete {
            removeEvents.onNext(notification)
        }
    }

    fun markAsRead(notification: Notification): Observable<Unit> {
        return notificationAPI.markAsRead(notification.id).doOnComplete {
            readEvents.onNext(notification)
        }
    }

    sealed class NotificationListUpdate {
        data class NewPageUpdate(
            val notifications: List<Notification>
        ) : NotificationListUpdate()

        data class MarkedAsReadUpdate(
            val notification: Notification
        ) : NotificationListUpdate()

        data class RemovedUpdate(
            val notification: Notification
        ) : NotificationListUpdate()
    }
}
