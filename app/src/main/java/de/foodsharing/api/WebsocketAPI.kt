package de.foodsharing.api

import de.foodsharing.model.MessageResponse
import io.reactivex.Observable
import java.util.Date

interface WebsocketAPI {

    interface Message

    data class OldConversationMessage(
        val id: Int,
        val cid: Int,
        val fsId: Int,
        val fsName: String,
        val fsPhoto: String?,
        val body: String,
        val time: Date
    ) : Message {
        fun toConversationMessage(): ConversationMessage {
            return ConversationMessage(
                cid,
                MessageResponse(id, body, time, fsId)
            )
        }
    }

    data class ConversationMessage(
        val cid: Int,
        val message: MessageResponse
    ) : Message

    fun subscribe(): Observable<Message>
    fun close()
}
