package de.foodsharing.api

import de.foodsharing.model.ContentEntry
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Retrofit API interface for
 */
interface ContentAPI {

    enum class ContentMode {
        HTML,
        MARKDOWN,
        PLAIN
    }

    enum class ContentID(val value: Int, val mode: ContentMode) {
        PRIVACY_STATEMENT(28, ContentMode.HTML);
    }

    /**
     * Requests details of a food basket.
     */
    @GET("/api/content/{id}")
    fun get(@Path("id") id: Int): Observable<ContentEntry>
}
