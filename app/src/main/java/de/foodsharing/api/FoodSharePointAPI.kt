package de.foodsharing.api

import de.foodsharing.model.FoodSharePoint
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FoodSharePointAPI {
    @GET("/api/foodSharePoints/{id}")
    fun get(@Path("id") id: Int): Observable<FoodSharePoint>

    @GET("/api/foodSharePoints/nearby")
    fun getNearby(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("distance") distance: Int
    ): Observable<List<FoodSharePoint>>
}
