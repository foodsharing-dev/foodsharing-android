package de.foodsharing.api

import de.foodsharing.model.Notification
import io.reactivex.Observable
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit API interface for bell notifications.
 */
interface NotificationAPI {

    @GET("/api/bells")
    fun list(
        @Query("limit") number: Int,
        @Query("offset") offset: Int = 0
    ): Observable<List<Notification>>

    @PATCH("/api/bells/{id}")
    fun markAsRead(@Path("id") id: Int): Observable<Unit>

    @DELETE("/api/bells/{id}")
    fun remove(@Path("id") id: Int): Observable<Unit>
}
