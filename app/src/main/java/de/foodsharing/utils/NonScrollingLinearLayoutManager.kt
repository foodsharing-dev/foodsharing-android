package de.foodsharing.utils

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager

class NonScrollingLinearLayoutManager(context: Context?) : LinearLayoutManager(context) {
    override fun canScrollVertically(): Boolean {
        return false
    }
}
