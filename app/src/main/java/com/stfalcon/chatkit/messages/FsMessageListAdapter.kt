package com.stfalcon.chatkit.messages

import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.utils.DateFormatter
import de.foodsharing.ui.conversation.ChatkitMessage
import java.util.Date

/**
 * Adapter for {@link MessagesList}.
 */
class FsMessageListAdapter(
    senderId: String,
    imageLoader: ImageLoader
) : MessagesListAdapter<ChatkitMessage>(senderId, imageLoader) {
    /**
     * Updates the list of items without calling notify internally. Can be used in conjunction with
     * a custom ListDiffer.
     */
    fun setItemsWithoutNotify(items: List<Wrapper<*>>) {
        this.items = items
    }

    fun wrapItemsBasedOnCurrent(items: List<ChatkitMessage>): List<Wrapper<*>> {
        val oldSelected = this.items
            .filter { it.isSelected }
            .mapNotNull { (it.item as? ChatkitMessage)?.id }
        val newItems = generateNewDateHeaders(items)

        newItems.filter { (it.item as? ChatkitMessage)?.id in oldSelected }
            .forEach { it.isSelected = true }

        @Suppress("UNCHECKED_CAST")
        return newItems
    }

    private fun generateNewDateHeaders(messages: List<ChatkitMessage>): List<Wrapper<*>> {
        val items = mutableListOf<Wrapper<*>>()
        for (i in messages.indices) {
            val message: ChatkitMessage = messages[i]
            items.add(Wrapper<ChatkitMessage>(message))
            if (messages.size > i + 1) {
                val nextMessage: ChatkitMessage = messages[i + 1]
                if (!DateFormatter.isSameDay(message.createdAt, nextMessage.createdAt)) {
                    items.add(Wrapper<Date>(message.createdAt))
                }
            } else {
                items.add(Wrapper<Date>(message.createdAt))
            }
        }
        return items
    }
}
