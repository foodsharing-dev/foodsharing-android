Closes ___ (e.g. #230)

## What does this MR do?

(briefly describe what this MR is about)

## Links to related issues

## Checklist

- [ ] no unrelated changes
- [ ] joined #fs-dev-android-beta channel at https://slackin.yunity.org
- [ ] added an entry to CHANGELOG.md
- [ ] increased the versionCode in /app/build.gradle
